# SOR-Semaforos-1S-2021
##  Trabajo Práctico Semáforos primer semestre año 2021
### Lectura de archivo
**En este caso se necesita leer la receta desde un archivo y la solución fue la siguiente.**
```
        char linea[50];
        FILE *leer_receta;
        leer_receta = fopen("receta.txt", "r");
        for (int i = 0; i < 8; i++)                  // Leo cada linea del archivo "receta"
        {
                fgets (linea, 50, leer_receta);      // Lee el buffer hasta que halla una "\n"
                escribir_en_archivo(linea,0);        // Escribe la receta en el archivo "subwayArgentoGanador.txt"
        }
```
### Escritura en archivo
**Para dicho propósito se utilizó la siguiente función.**
```
void escribir_en_archivo(char msg[], int equipo)
{
        FILE* competencia;
        competencia = fopen("subwayArgentoGanador.txt", "a");
        fprintf (competencia,msg,equipo);
        fclose(competencia);
}
```
### Sincronización
**Para controlar la utilización del equipamiento compartido se utilizaron los siguientes semáforos.**
- sem_t sem_mezclar;
- sem_t sem_salero;
- sem_t sem_sarten;
- sem_t sem_horno;
- sem_t sem_armar_sandwich;
- static pthread_mutex_t m_salero;
- static pthread_mutex_t m_sarten;
- static pthread_mutex_t ganador;

### Dificultades
La mayor dificultad encontrada fue al momento de escribir las acciones de cada equipo en un archivo, la solución encontrada fue la de eliminar las estructuras y modificar la función llamada ***"ejecutarReceta"*** ya que al momento de escribir las acciones de los diferentes equipos dentro del archivo no cumplía con lo solicitado. Una solución a esto fue la de en primer lugar leer el archivo que contenía la receta y luego con la función ***"strstr"*** la cual busca cierta subcadena dentro de una cadena de texto y si la subcadena esta en la cadena retorna un puntero a la posición en que se encuentra, o en caso de no contenerla retorna *NULL*.
#### Ejemplo:
```
if (strstr(linea, "salar") != NULL)
{
        salar(equipo);
}
```
